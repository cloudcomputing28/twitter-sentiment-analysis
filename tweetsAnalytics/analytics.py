#!flask/bin/python
import json
import socket
import couchdb
import unirest
from flask import Flask
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app)
couch = couchdb.Server('http://Jay:admin@130.56.248.247:5984')

def getAPIresponse(urldata):
    response = unirest.post(
        "https://twinword-sentiment-analysis.p.mashape.com/analyze/",
        headers={
            "X-Mashape-Key": "IzkzAen98Kmshn3DlXEmYWyC4eK9p1mC9WfjsnVae0oLHwXjHz",
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        },
        params={
            "text": urldata
        }
    )
    return response.body.get("score")

@app.route('/')
def index():
    # db = couch['sentimentsresult']
    # map_fun = '''function (doc) {
    #     emit(doc.city, doc.sentiment);
    # }'''
    # reduce_fun = '''function (keys, values, rereduce) {
    #     avg = sum(values)/values.length;
    #     return(avg);
    # }'''
    # response = db.query(map_fun, reduce_fun, group=True)
    response = unirest.get("http://130.56.248.247:5984/sentimentsresult/_design/" +
                           "avg_sentiment/_list/sort/avg_sentiment?group=true")
    jsonresponse = json.dumps(response.body.get('results'))
    # jsonresponse = json.dumps(response.rows)
    return jsonresponse

@app.route('/process')
def process():
    row_db = couch['tweets']
    map_fun = '''function (doc) {
        emit(doc.name, doc.text);
    }'''

    try:
        newdb = couch['sentimentsresult']
    except:
        newdb = couch.create('sentimentsresult')

    placeresults = row_db.query(map_fun)
    for row in placeresults:
        sentimentindex = getAPIresponse(row.value)
        doc = {'city': row.key, 'sentiment': sentimentindex}
        newdb.save(doc)
        del row_db[row.id]

    return "Process completed!"

@app.route('/test_lb')
def test_lb():
    html = "<b>Hostname:</b> {hostname}<br/>"
    return html.format(hostname=socket.gethostname())

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=100, debug=True)

