#!/bin/bash
# Dynamically scale up/down middleware instances on Nectar basedon
# CPU usage.
# - Author: Sangzhuoyang Yu
# - Student Id: 747838

rm log
while true
do
    usage=$(echo $[100-$(vmstat 1 2|tail -1|awk '{print $15}')])
    # convert to int
    usage=${usage%.*}
    instName=$(echo sent-analysis2)
    instStatus=$(docker node ls | grep "$instName " | grep -o "Ready")

    # echo "CPU usage: $usage%" >> log
    if [[ $usage -gt 15 && $instStatus != "Ready" ]]
    then
        # Add the node to swarm
        docker-machine ssh $instName "docker swarm join $1 130.56.249.175:2377" >> log
        # Scale up service
        docker stack deploy -c docker-compose.yml sentiment-analysis >> log
        echo "CPU usage: $usage%" >> log
        echo "$instName activated" >> log
        sleep 30
    elif [[ $usage -le 8 && $instStatus == "Ready" ]]
    then
        # Drain a worker node
        docker node update --availability drain $instName >> log
        # Remove the node from swarm
        docker-machine ssh $instName "docker swarm leave" >> log
        while [[ $instStatus != "Down" ]]
        do
            sleep 5
            instStatus=$(docker node ls | grep "$instName " | grep -o "Down")
        done
        docker node rm $instName >> log
        # Scale down service
        docker stack deploy -c docker-compose-small.yml sentiment-analysis >> log
        echo "CPU usage: $usage%" >> log
        echo "$instName deactivated" >> log
        sleep 10
    fi
    sleep 5
done &


