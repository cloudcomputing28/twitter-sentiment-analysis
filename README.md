# Twitter sentiment analysis

* Explore relationships between personal income and sentiments towards immigration through tweets analysis and data visualization

## File structures

        ./front           // Data visualization website
        ./deployment      // Deployment scripts
        ./tweetsAnalytics // Twitter sentiment analysis backend module
        ./tweetsHarvest   // Twitter harvesting backend module
        ./JMeterLoadTest  // Backend load testing scripts

## Deployment

* Isolated deployment with Docker containers
* Load balancing and auto-scaling with Docker swarm
* Auto-scaling by monitoring the cpu resources use
* Automate command execution with Ansible

### Local configureation
1. Install Ansible with apt:

        sudo apt-get install software-properties-common
        sudo apt-add-repository ppa:ansible/ansible
        sudo apt-get update
        sudo apt-get install ansible

2. Place SSH private key under /ssh directory.

### Server configuration
1. Login into cloud instances with ssh key located in the deployment folder.
2. Copy SSH public key content to root dir:

        sudo cp /home/ubuntu/.ssh/authorized_keys /root/.ssh/

3. Enable Docker machine to login with root user through SSH:

    a. Modify /etc/ssh/sshd_config:

        set PermitRootLogin to yes.
    
    b. Restart ssh daemon service:

        sudo service sshd restart
    
    c. Ensure the following ports are opened:

    * TCP port 2376: for secure Docker client communication. This port is required for Docker Machine to work. Docker Machine is used to orchestrate Docker hosts.
    * TCP port 2377: for communication between the nodes of a Docker Swarm or cluster. It only needs to be opened on manager nodes.
    * TCP and UDP port 7946: for communication among nodes (container network discovery).
    * UDP port 4789: for overlay network traffic (container ingress networking).

4. Install docker-machine:

        curl -L https://github.com/docker/machine/releases/download/v0.10.0/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine && chmod +x /tmp/docker-machine && sudo cp /tmp/docker-machine /usr/local/bin/docker-machine

### Run Deployment Tasks
1. Go to ~/deployment directory.
2. Execute Ansible playbook (clearing cmd history is recommand for safety):

        sudo ansible-playbook -i inventory deploy_middleware_sent.yml --extra-vars "ansible_become_pass=<yourRootPassword>"
