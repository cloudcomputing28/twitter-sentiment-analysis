#!/usr/bin/python
# -*- coding: UTF-8 -*-

from twitter import *
import couchdb
import time
import re

couch = couchdb.Server('http://Jay:admin@130.56.248.247:5984/')

try:
    db = couch.create('tweets')
except:
    db = couch['tweets']

#Application authentication
auth = OAuth(
            consumer_key='zmOGVpDesAAgfdSvP8PQPq975',
            consumer_secret='r6YHTD3GIgJrZ6fv2iK8GA8pBZdLIZdvKsfgFlVDZS7i78Nn77',
            token='1674989683-OwwX0WYnjqljUEuhTkSXOb51g53kn99UrzwixM2',
            token_secret='pqpGuZ3ns0gsCEJZoYM6XqeztOL04WzITo5CdVeOTFznx'
            )

#Harvest tweets by Search api
#Limit: 450 request per 15 mins, 100 tweets per request, up to 10 days older
keywords = 'migrat OR immigrat OR emigrant OR migrant OR emigrat OR expatriate \
OR settler OR emigre OR noncitizen OR guestworker'
#Terminate after go through 10000 tweets
request_count = 100
current_id = 0
twitter = Twitter(auth = auth)
first = twitter.search.tweets(  q = keywords,
                                lang = 'en',
                                count = 1,
                                geocode = '-28.53,134.64,2200km' )
current_id = first['search_metadata']['max_id']
#ALICE SPRING: -25.6,134.35,2500km
while request_count:
    tweets = twitter.search.tweets( q = keywords,
                                    lang = 'en',
                                    count = 100,
                                    geocode = '-28.53,134.64,2200km',
                                    max_id = current_id )
    for tweet in tweets['statuses']:
        current_id = tweet['id']
        if tweet['user']['location'] != '':
            try:
                db[tweet['id_str']] = {
                                        'text':tweet['text'],
                                        'name':tweet['user']['location']
                                        }
            except:
                pass
    current_id -= 1
    request_count -= 1
    time.sleep(10)


#Harvest tweets by Streaming api
keywords = 'migrat|immigrat|emigrant|migrant|expatriate|emigrat|settler|emigre'
#Terminate after go through tweets
stream_count = 10000
twitter_stream = TwitterStream(auth=auth)
tweets = twitter_stream.statuses.filter(locations= '110.76,-43.37,155.4,-10.24',
                                        language= 'en')
for tweet in tweets:
    stream_count -= 1
    if 'text' in tweet:
        if 'location' in tweet['user']:
            if re.search(keywords,tweet['text']):
                try:
                    db[tweet['id_str']] = {
                                            'text':tweet['text'],
                                            'name':tweet['user']['location']
                                            }
                except:
                    pass
    if stream_count == 0:
        break
