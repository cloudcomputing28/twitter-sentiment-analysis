#!/usr/bin/python
# -*- coding: UTF-8 -*-

import re
from twitter import *
import couchdb
from flask import Flask

app = Flask(__name__)
couch = couchdb.Server('http://Jay:admin@130.56.248.247:5984/')

@app.route('/')
def index():
    try:
        db = couch.create('tweets')
    except:
        db = couch['tweets']

    #Application authentication
    auth = OAuth(
        consumer_key='zmOGVpDesAAgfdSvP8PQPq975',
        consumer_secret='r6YHTD3GIgJrZ6fv2iK8GA8pBZdLIZdvKsfgFlVDZS7i78Nn77',
        token='1674989683-OwwX0WYnjqljUEuhTkSXOb51g53kn99UrzwixM2',
        token_secret='pqpGuZ3ns0gsCEJZoYM6XqeztOL04WzITo5CdVeOTFznx'
    )

    #Harvest tweets by Streaming api
    keywords = 'migrate|immigration|emigrant|migrant|immigrating|immigrate|\
    emigration|expatriate|emigrating|emigrate|settler|migration|migrating|emigre|\
    noncitizen|guestworker'
    stream_count = 10000
    twitter_stream = TwitterStream(auth=auth)
    tweets = twitter_stream.statuses.filter(
        locations='113.76,-38.37,153.4,-12.24',
        language='en')
    for tweet in tweets:
        if 'text' in tweet:
            if 'place' in tweet:
                if re.search(keywords, tweet['text']):
                    try:
                        db[tweet['id_str']] = {
                            'text': tweet['text'],
                            'name': tweet['place']['name'],
                            'full_name': tweet['place']['full_name']
                        }
                        stream_count -= 1
                    except:
                        pass
        if stream_count == 0:
            break

    return "Tweets harvesting completed"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=100, debug=True)

